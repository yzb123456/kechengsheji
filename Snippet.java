package chess;

import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

class FivChese  extends JFrame{//继承自窗口类
    CheseGame CG;
    FivChese ()
    {
        this.CG= new CheseGame();
        addMouseListener(CG);//鼠标监听器
        add(CG);
    }
    public static void main(String[] args) {
        FivChese  fivechese= new FivChese ();
        fivechese.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//设成退出程序，相当于是System.exit(0)
        fivechese.setSize(900, 700);//设置组件的长宽大小
        fivechese.setTitle("五子棋");//设置组建的自定义标题
        fivechese.setVisible(true);//显示按钮
       // fivechese.setResizable(true);//参数为boolean类型，resizeable值为true时，表示在生成的窗体可以自由改变大小；
    }
}
class CheseGame extends JPanel  implements MouseListener,ItemListener//图形用户界面工具包swing中的面板容器类，可以加入JFrame窗口中
{
    int [][]map = new int[20][20];
    Color []color = {Color.BLACK,Color.WHITE,Color.LIGHT_GRAY};//背景颜色
    //棋子坐标
    int x = -1;
    int y = -1;
    //判定是白棋还是黑棋
    int flag = 1;
    //判定胜负
    int winner = 3;
   
    JButton btn1 = new JButton("开始"); //游戏开始
    JButton btn2 = new JButton("再来一局");//游戏重置
    JButton btn3 = new JButton("悔棋");
    //确定白棋先还是黑棋先
    CheckboxGroup cbg = new CheckboxGroup();
    Checkbox  chb1 = new Checkbox("黑旗先开始",cbg,true);
    Checkbox  chb2 = new Checkbox("白旗先开始",cbg,false);
    class GameStart implements ActionListener//游戏开始继承自动作监听器
    {
        public void actionPerformed(ActionEvent ae)//编写一个方法处理ActionEvent事件
        {
             if(ae.getSource() == btn1)//str.equals("Start")
             {
                 start();
                 repaint();//重新描绘component的方法
                 btn1.setEnabled(false);//设置控件是否可用
                 btn2.setEnabled(true);
                 btn3.setEnabled(true);
                 chb1.setEnabled(false);
                 chb2.setEnabled(false);
             }
             if(ae.getSource() == btn2)//处理再来一局事件
             {
                 start();
                 repaint();
                 chb1.setEnabled(true);
                 chb2.setEnabled(true);
             }
             if(ae.getSource() == btn3)//处理悔棋事件
             {
                 repaint();
                 chb1.setEnabled(false);
                 chb2.setEnabled(false);
             }
        }

		private void start() {
			// TODO Auto-generated method stub
			
		}
    }
    CheseGame()
    {  
        this.setLayout(null);//
        chb1.setBounds(650, 50, 100, 30);//设置开始控件的位置
        chb2.setBounds(650, 100, 100, 30);
        chb1.addItemListener(this);
        chb2.addItemListener(this);
        btn1.setBounds(650,150,100,30);
        btn1.addActionListener(new GameStart()); //建立一个新的游戏开始对象
        btn2.setBounds(650, 200, 100, 30);
        btn2.addActionListener(new GameStart());
        btn2.setEnabled(false);
        btn3.setBounds(650, 250, 100, 30);
        btn3.addActionListener(new GameStart());
        this.add(btn1);
        this.add(btn2);
        this.add(btn3);
        this.add(chb1);
        this.add(chb2);
    }
		
    public void paintComponent(Graphics g)
		       {
		             super.paintComponent(g);//是父类JPanel里面的方法，会把整个面板用背景颜色重画一遍，起到清屏作用
		             setBackground(color[2]);//设置棋盘背景颜色
		             //画棋盘
		             for(int i = 1;i <= 20;i++)
		             {
		                 g.setColor(color[0]);
		                 g.drawLine(30, i*30 ,600 , i*30);//画线绘制棋盘格子，这是画横线的，30毫米画一条直线，从30到600
		                 g.drawLine(i*30,30, i*30, 600);//画棋盘竖线
		             }
		             //画棋子
		             for(int i = 0;i < 20;i++)
		                 for(int j = 0;j < 20;j++)
		                 {
		                     if(map[i][j] == 1)
		                     { 
		                         g.setColor(color[0]);//画棋子颜色
		                         g.fillOval((i+1)*30-15, (j+1)*30-15, 30, 30);
		                     }
		                     if(map[i][j] == 2)
		                     {
		                         g.setColor(color[1]);
		                         g.fillOval((i+1)*30-15, (j+1)*30-15, 30, 30);//描绘棋子
		               }
		                 
		       }
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void itemStateChanged(ItemEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}

